package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.*;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class SensorView extends JPanel implements Observer{
    private ISensor sensor;

    private JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");

    public SensorView(ISensor c) {
        this.sensor = c;
        this.setLayout(new BorderLayout());
        
        final Commande onCommande = new CommandeOn(sensor);
        final Commande offCommande = new CommandeOff(sensor);
        final Commande acquireCommande = new CommandeAcquire(sensor);

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
					execute(onCommande);
				} catch (SensorNotActivatedException e1) {
					e1.printStackTrace();
				}
            }
        });

        off.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	try {
					execute(offCommande);
				} catch (SensorNotActivatedException e1) {
					e1.printStackTrace();
				}
            }
        });

        update.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	try {
					execute(acquireCommande);
				} catch (SensorNotActivatedException e1) {
					e1.printStackTrace();
				}
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);

        this.add(buttonsPanel, BorderLayout.SOUTH);
    }

	public void update(Observable arg0, Object arg1) {
		value.setText(arg1 + "");
	}
	
	public void execute(Commande cmd) throws SensorNotActivatedException {
	      cmd.execute();        
	   }
}
