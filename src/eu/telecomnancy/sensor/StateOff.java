package eu.telecomnancy.sensor;

public class StateOff implements State{

	private final TemperatureSensor temperatureSensor;

	public StateOff(TemperatureSensor sensor){
		this.temperatureSensor = sensor;
	}
	
	public boolean getStatus(){
		return false;
	}

	public void on() {
		temperatureSensor.setStatus(new StateOn(temperatureSensor));
	}

	public void off() {

	}

	public void update() throws SensorNotActivatedException {
		throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	}

	public double getValue() throws SensorNotActivatedException{
		throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}
}
