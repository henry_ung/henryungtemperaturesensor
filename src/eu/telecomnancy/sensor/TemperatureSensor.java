package eu.telecomnancy.sensor;

import java.util.*;

public class TemperatureSensor extends Observable implements ISensor {
    
    boolean state;
    double value = 0;
    State status;
    
    public TemperatureSensor(){
        status = new StateOff(this);
    }
    
    public void on() {
        status.on();
    }
    
    public void off() {
        status.off();
    }
    
    public boolean getStatus() {
        return status.getStatus();
    }
    
    public void setStatus(State status) {
        this.status = status;
    }
    
    public void update() throws SensorNotActivatedException {
        status.update();
        setChanged();
        notifyObservers(value);
    }
    
    public double getValue() throws SensorNotActivatedException {
        return status.getValue();
    }
    
}
