package eu.telecomnancy.sensor;

import java.util.Random;

public class StateOn implements State{
	
	private final TemperatureSensor temperatureSensor;
	
	public StateOn(TemperatureSensor sensor){
		this.temperatureSensor = sensor;
	}
	
	public boolean getStatus(){
		return true;
	}

	public void on() {
		
	}

	public void off() {
		temperatureSensor.setStatus(new StateOff(temperatureSensor));
	}

	public void update() throws SensorNotActivatedException{
		temperatureSensor.value = (new Random()).nextDouble() * 100;
	}

	public double getValue() throws SensorNotActivatedException{
		return temperatureSensor.value;
	}

}
