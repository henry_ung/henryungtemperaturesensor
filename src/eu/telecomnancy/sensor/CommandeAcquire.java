package eu.telecomnancy.sensor;

public class CommandeAcquire implements Commande{

	ISensor sensor;
	
	public CommandeAcquire(ISensor s){
		this.sensor = s;
	}

	public void execute() throws SensorNotActivatedException {
		sensor.update();
	}

}
