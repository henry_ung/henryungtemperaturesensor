package eu.telecomnancy.sensor;

public class TemperatureSensorAdapter implements ISensor{

	private LegacyTemperatureSensor oldsensor;
	
	public TemperatureSensorAdapter(LegacyTemperatureSensor oldsensor){
		this.oldsensor = oldsensor;
	}

	public void on() {
		if (!oldsensor.getStatus()){
			oldsensor.onOff();
		}
	}

	public void off() {
		if (oldsensor.getStatus()){
			oldsensor.onOff();
		}
	}

	public boolean getStatus() {
		return oldsensor.getStatus();
	}

	public void update() throws SensorNotActivatedException {
		if (this.getStatus()) {
			this.off();
			this.on();
		} else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	}

	public double getValue() throws SensorNotActivatedException {
		if (this.getStatus())
			return oldsensor.getTemperature();
		else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}
}
