package eu.telecomnancy.sensor;

public abstract class TemperatureDecorator extends TemperatureSensor{
	
	ISensor sensor;
	
	public abstract double getValue() throws SensorNotActivatedException;
}
