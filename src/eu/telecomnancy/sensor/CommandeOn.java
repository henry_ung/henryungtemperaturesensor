package eu.telecomnancy.sensor;

public class CommandeOn implements Commande{

	ISensor sensor;
	
	public CommandeOn(ISensor s){
		this.sensor = s;
	}

	public void execute() throws SensorNotActivatedException {
		sensor.on();
	}

}
