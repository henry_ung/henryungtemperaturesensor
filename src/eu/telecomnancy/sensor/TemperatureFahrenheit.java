package eu.telecomnancy.sensor;

public class TemperatureFahrenheit extends TemperatureDecorator{
	
	public TemperatureFahrenheit(ISensor sensor){
		this.sensor = sensor;
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		System.out.println();
		return this.sensor.getValue() * 1.8 + 32;
	}

}
