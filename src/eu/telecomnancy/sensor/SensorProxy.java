package eu.telecomnancy.sensor;

public class SensorProxy implements ISensor {
    private ISensor sensor;
    private SensorLogger log;

    public SensorProxy(ISensor _sensor, SensorLogger sensorLogger) {
        sensor = _sensor;
        log = sensorLogger;
    }

    public void on() {
        log.log(LogLevel.INFO, "Sensor On");
        sensor.on();
    }

    public void off() {
        log.log(LogLevel.INFO, "Sensor Off");
        sensor.off();
    }

    public boolean getStatus() {
        log.log(LogLevel.INFO, "Sensor getStatus");
        return sensor.getStatus();
    }

    public void update() throws SensorNotActivatedException {
        log.log(LogLevel.INFO, "Sensor update");
        sensor.update();
    }

    public double getValue() throws SensorNotActivatedException {
        log.log(LogLevel.INFO, "Sensor value =" + sensor.getValue());
        return sensor.getValue();
    }
}
