package eu.telecomnancy.sensor;

public class CommandeOff implements Commande{

	ISensor sensor;
	
	public CommandeOff(ISensor s){
		this.sensor = s;
	}
	
	public void execute() {
		sensor.off();
	}

}
