package eu.telecomnancy.sensor;

public class TemperatureArrondie extends TemperatureDecorator{
	
	public TemperatureArrondie(ISensor sensor){
		this.sensor = sensor;
	}

	@Override
	public double getValue() throws SensorNotActivatedException{
		double res = this.sensor.getValue();
		return Math.round(res);
	}
}
