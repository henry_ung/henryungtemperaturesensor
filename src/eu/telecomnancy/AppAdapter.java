package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.TemperatureSensorAdapter;
import eu.telecomnancy.ui.ConsoleUI;

public class AppAdapter {

    public static void main(String[] args) {
    	LegacyTemperatureSensor oldsensor = new LegacyTemperatureSensor();
        ISensor sensor = new TemperatureSensorAdapter(oldsensor);
        new ConsoleUI(sensor);
    }

}